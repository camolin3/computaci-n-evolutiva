#define SOME_RESULTS

using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace UC
{
	public class GA
	{
		protected Individual[] oldPop, newPop;
		protected ObjectiveFunction fitness;
		protected Individual incumbent;
		protected Individual bestInGeneration;
		protected Random sel;
		protected int generation;
		protected int maxGenerations;
		protected int maxPopulation;
		protected Stopwatch sw;
		
		public GA (ObjectiveFunction fitness, int maxGenerations, int maxPopulation)
		{
			this.fitness = fitness;
			this.maxGenerations = maxGenerations;
			this.maxPopulation = maxPopulation;
			this.sel = new Random ();
			this.sw = new Stopwatch ();
		}
		
		/// <summary>
		/// Inicia la ejecución del Algoritmo Genético.
		/// </summary>
		public Individual Start ()
		{
			generation = 0;
			Console.WriteLine ("Inicio del algoritmo genético");
			Console.WriteLine ("-----------------------------");
			sw.Start ();
			
			oldPop = Initialize ();
			
			do {
				generation++;
				Generation ();
				Report ();
				oldPop = newPop;
			} while (Continue());
			
			sw.Stop ();
			Console.WriteLine ("-----------------------------------");
			Console.WriteLine ("Finalización del algoritmo genético");
			Console.WriteLine ("Tiempo total de ejecución: {0} minutos, {1} segundos", sw.Elapsed.Minutes, sw.Elapsed.Seconds);
			
			return incumbent;
		}
		
		/// <summary>
		/// Crea una nueva generación, usa Selección, Crossover y Mutación
		/// </summary>
		protected void Generation ()
		{
			// Evalúo la población
			EvaluatePopulation ();
			
			newPop = new Individual[oldPop.Length];
			List<Individual> childrens;
			// Supongamos la población es par
			for (int i=0; i<oldPop.Length; i+=childrens.Count) {
				// Selección
				// Ordeno las soluciones si se necesita para el ranking
				if (2*maxGenerations / 3 <= generation) Array.Sort (oldPop);
				Individual i1 = Select ();
				Individual i2 = Select ();
				
				// Crossover y mutación
				childrens = Individual.Crossover (i1, i2);
				
				for (int j=0; j<childrens.Count; j++)
					newPop [i + j] = childrens [j];
			}
		}
		
		/// <summary>
		/// Selecciona un individuo al azar para la próxima generación
		/// </summary>
		protected Individual Select ()
		{
			if (2*maxGenerations / 3 > generation) {
				// Por ahora, sólo torneo
				// Selecciono 2 individuos al azar
				int s1 = sel.Next (oldPop.Length), s2 = sel.Next (oldPop.Length); 
				// Me quedo con el mejor
				return (oldPop [s1] >= oldPop [s2]) ? oldPop [s1] : oldPop [s2];
			} else {
				// Ahora, ranking
				double s = sel.NextDouble ();
				double n_nPlus1 = oldPop.Length * (oldPop.Length + 1);
				int i;
				
				double cF = 0;
				for (i=1; i<=oldPop.Length; i++) {
					cF += (2.0 * i) / n_nPlus1;
					if (s <= cF)
						break;
				}
				return oldPop [i-1];
			}
		}
		
		protected void Report ()
		{
#if(SOME_RESULTS)
			if (generation % 250 == 0)
#endif
				Console.WriteLine ("Mejor generación #{0}: {1:f2}   -   Mejor global: {2:f2}", generation, bestInGeneration.Fitness, incumbent.Fitness);
		}
		
		/// <summary>
		/// Evaluates the population, repairing broken solutions.
		/// </summary>
		private void EvaluatePopulation ()
		{
			// Reseteo el mejor visto en la generación
			bestInGeneration = null;
			
			foreach (Individual i in oldPop) {
				// Actualizo los valores, y guardo el máximo
				fitness.Fitness (i);
				// Si es el mejor de la generación, lo guardo
				if (bestInGeneration == null || i > bestInGeneration) {
					bestInGeneration = i;
					// Si además es el mejor de todas las generaciones anteriores...
					if (incumbent == null || i > incumbent)
						incumbent = i;
				}
			}
		}
		
		protected Individual[] Initialize ()
		{
			// Creo	la población inicial
			Individual[] list = new Individual[maxPopulation];
			for (int s=0; s<maxPopulation; s++)
				// Agrego individuos random
				list [s] = Individual.GenerateRandomIndividual ();
			return list;
		}
		
		protected bool Continue ()
		{
			return maxGenerations > generation;
		}
	}
}

