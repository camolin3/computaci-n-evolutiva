using System;
using System.Collections.Generic;

namespace UC
{
	public class Individual : IComparable
	{
		public int[] Chromosome { get; set; }
		public double Fitness { get; set; }
		
		public static int Values { get; set; }
		public static Random cut;
		public static Random pC;
		public static Random mut;
		public static Random pM;
		protected static double pCrossover;
		protected static double pMutation;

		public static int students;
		public static int maxFamilyIncome;
		public static int maxFamilyIncomeFullGrant;
		public static double minGrade;
		
		public Individual (int[] chromosome)
		{
			this.Chromosome = chromosome;
		}
		
		/// <summary>
		/// Crossover the specified Individuals i1 and i2, using mutations.
		/// </summary>
		/// <param name='i1'>
		/// Primer padre.
		/// </param>
		/// <param name='i2'>
		/// Segundo padre.
		/// </param>
		public static List<Individual> Crossover (Individual i1, Individual i2)
		{
			// Supongo que 2 padres generan 2 hijos
			List<Individual> childrens = new List<Individual> (2);
			// Tiro una moneda y veo si se hace crossover, o pasan los individuos tal cuál
			// TODO Cambiar el orden de los estudiantes, de modo que el crossover tenga un pto de corte más inteligente que "1"
			int at = (pM.NextDouble() <= pCrossover) ? cut.Next (1, students - 1) : students - 1;
			int[] sc1 = new int[students], sc2 = new int[students];
			
			// Cada número copiado tiene una probabilidad de mutar
			
			for (int s=0; s<=at; s++) {
				sc1 [s] = Mutate(i2, s);
				sc2 [s] = Mutate(i1, s);
			}
			for (int s=at+1; s<students; s++) {
				sc1 [s] = Mutate(i1, s);
				sc2 [s] = Mutate(i2, s);
			}
			
			childrens.Add (new Individual (sc1));
			childrens.Add (new Individual (sc2));
			
			return childrens;
		}
		
		#region Static methods
		/// <summary>
		/// Mutate the specified individual i, at some position pos.
		/// </summary>
		/// <param name='i'>
		/// Individuo de donde se obtiene la información genética.
		/// </param>
		/// <param name='pos'>
		/// Posición que se está copiando.
		/// </param>
		private static int Mutate(Individual i, int pos)
		{
			int copy = i.Chromosome[pos];
			// Si ocurre la mutación
			int maxValue = Elegible(pos);
			if(pM.NextDouble()<pMutation && maxValue>1){
				// Muto: No puedo sumar 0, así que parto de 1
				copy += mut.Next(1, maxValue);
				// Normalizo en caso de "pasarme"
				copy %= maxValue;
				// No tiene sentido mutar a 0, será una peor solución
				copy = copy == 0 ? 1 : copy;
			}
			
			return copy;
		}
				
		/// <summary>
		/// Genera una asignación de becas que respeta algunas restricciones.
		/// </summary>
		/// <returns>
		/// Un individuo Grant (que no revisa si se pasa del presupuesto).
		/// </returns>
		public static Individual GenerateRandomIndividual ()
		{
			int[] grants = new int[students];
		
			for (int i=0; i<students; i++) {
				// Revisa restricción 
				int maxValue = Elegible (i);
				if (maxValue > 1) {
					grants [i] = cut.Next (maxValue);
				}
			}
		
			Individual d = new Individual (grants);
			return d;
		}
	
		/// <summary>
		/// ¿Cuánta beca se le puede asignar al i-ésimo alumno?
		/// </summary>
		/// <param name='i'>
		/// El máximo valor a tomar, no inclusivo.
		/// </param>
		private static int Elegible (int i)
		{
			int maxValue = 1; // Es decir, 0
			if(MainClass.data_grades [i] >= minGrade && MainClass.data_familyIncomes [i] <= maxFamilyIncome) // Si cumple con las notas y el ingreso máximo
			{
				maxValue = 3; // Es decir, 2
				if(MainClass.data_familyIncomes[i]>maxFamilyIncomeFullGrant) // Si ya no puede obtener beca completa por alto ingreso
					maxValue = 2; // Es decir, 1
			}
			return maxValue;
		}

		/// <summary>
		/// Setea los parámetros estáticos.
		/// </summary>
		public static void SetParameters (double pCrossover, double pMutation, int values, double minGrade, int maxFamilyIncome, int maxFamilyIncomeFullGrant)
		{	
			Individual.pCrossover = pCrossover;
			Individual.pMutation = pMutation;
			Individual.Values = values;
			Individual.students = MainClass.data_grades.Length;
			Individual.minGrade = minGrade;
			Individual.maxFamilyIncome = maxFamilyIncome;
			Individual.maxFamilyIncomeFullGrant = maxFamilyIncomeFullGrant;
			Individual.cut = new Random ();
			Individual.pC = new Random ();
			Individual.mut = new Random ();
			Individual.pM = new Random();
		}
		#endregion
		
		#region Override operators and methods
		public override string ToString ()
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder ();
		
			long totalAsignation = 0;
		
			sb.AppendLine();
			sb.Append ("ASIGNACION OPTIMA\n");
			for (int s=0; s<Chromosome.Length; s++) {
				sb.Append (MainClass.data_names [s]);
				sb.Append (", ");
			
				switch (Chromosome [s]) {
				case 0:
					sb.Append ("Nada");
					break;
				case 1:
					sb.Append ("Media");
					break;
				case 2:
					sb.Append ("Total");
					break;
				}
				sb.AppendLine ();
				totalAsignation += (long)(MainClass.data_tuitions [s] * (Chromosome [s] / 2.0));
			}
		
			sb.Append ("Valor total asignado: ");
			sb.Append (totalAsignation.ToString ("C"));
		
			return sb.ToString ();
		}
		
		public int CompareTo (object obj)
		{
			if(obj==null) return 1;
			
			Individual i1 = obj as Individual;
			if(i1!=null)
				return this.Fitness.CompareTo(i1.Fitness);
			else
				throw new ArgumentException("No era un objeto Individuo.");
		}
		
		public static bool operator > (Individual i1, Individual i2)
		{
			return i1.Fitness > i2.Fitness;	
		}
		
		public static bool operator < (Individual i1, Individual i2)
		{
			return i1.Fitness < i2.Fitness;	
		}
		
		public static bool operator >= (Individual i1, Individual i2)
		{
			return i1.Fitness >= i2.Fitness;	
		}
		
		public static bool operator <= (Individual i1, Individual i2)
		{
			return i1.Fitness <= i2.Fitness;	
		}
		#endregion
		
	}
}

