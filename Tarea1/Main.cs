using System;

namespace UC
{
		class MainClass
		{
				public static int generations;
				public static int population;
				public static double pCrossover;
				public static double pMutation;
				public static int maxBudget;
				public static int maxFamilyIncome;
				public static int maxFamilyIncomeFullGrant;
				public static double minGrade;
				public static string[] data_names;
				public static double[] data_familyIncomes;
				public static double[] data_grades;
				public static uint[] data_tuitions;
		
				public static void Main (string[] args)
				{	
						// Leo los archivos
						string configPath = (args.Length > 0) ? args [0] : "";
						ReadConfiguration (configPath);
						String data = (args.Length > 1) ? args [1] : "postulantes.txt";
						ReadData (data);
			
						InitialReport ();
			
						// Ingreso los parámetros
						Individual.SetParameters (pCrossover, pMutation, 3, minGrade, maxFamilyIncome, maxFamilyIncomeFullGrant);
						ObjectiveFunction function = new ObjectiveFunction (maxFamilyIncome, minGrade);
						GA ga = new GA (function, generations, population);
			
						// Comienzo el algoritmo genético
						Individual solution = ga.Start ();
			
						// Despliego el resultado
						Console.WriteLine (solution.ToString ());
						Console.Read ();
				}
		
				/// <summary>
				/// Reporte inicial con los parámetros con los que se ejecutará el algoritmo.
				/// </summary>
				private static void InitialReport ()
				{
						Console.WriteLine ("-------------------------------------------");
						Console.WriteLine ("|------Tarea 1 Computación evolutiva------|");
						Console.WriteLine ("|-----------Asignación de becas-----------|");
						Console.WriteLine ("-------------------------------------------");
						Console.WriteLine ();
			
						// Parámetros cargados
						Console.WriteLine ("\tParámetros cargados");
						Console.WriteLine ("\t-------------------");
						Console.WriteLine ("\tGeneraciones: {0:N}", generations);
						Console.WriteLine ("\tTamaño de la población: {0}", population);
						Console.WriteLine ("\tTasa de crossver: {0:G}", pCrossover);
						Console.WriteLine ("\tTasa de mutación: {0:G}", pMutation);
						Console.WriteLine ("\tPresupuesto máximo a asignar: {0:C}", maxBudget);
						Console.WriteLine ();
			
						// Restricciones de enunciado
						Console.WriteLine ("\tRestricciones de enunciado");
						Console.WriteLine ("\t--------------------------");
						Console.WriteLine ("\tMáximo sueldo capaz de acceder a beca completa: {0:C}", maxFamilyIncomeFullGrant);
						Console.WriteLine ("\tMáximo sueldo familiar para postular: {0:C}", maxFamilyIncome);
						Console.WriteLine ("\tNota mínima para postular: {0:G}", minGrade);
						Console.WriteLine ();
				}
		
		#region Reading and parsing files
				private static void ReadData (String data)
				{
						try {
								string[] lines = System.IO.File.ReadAllLines (data, System.Text.Encoding.GetEncoding ("iso-8859-1"));
			
								// Inicializo los arreglos
								data_names = new string[lines.Length];
								data_familyIncomes = new double[lines.Length];
								data_grades = new double[lines.Length];
								data_tuitions = new uint[lines.Length];
			
								for (int l=0; l<lines.Length; l++) {
										if (lines [0].Trim ().Length == 0)
												continue;
					
										string[] pieces = lines [l].Split (';');
				
										data_names [l] = pieces [0];
										data_familyIncomes [l] = Double.Parse (pieces [1]);
										data_grades [l] = Double.Parse (pieces [2]);
										data_tuitions [l] = UInt32.Parse (pieces [3]);
								}
						} catch (Exception e) {
								Console.WriteLine (e.Message);
						}
				}
		
				/// <summary>
				/// Leo la configuración desde un archivo (y si no está, setea parámetros predeterminados).
				/// </summary>
				/// <param name='path'>
				/// Ruta del archivo de configuración.
				/// </param>
				private static void ReadConfiguration (String path)
				{
						// Cargo dos restricciones del enunciado
						maxFamilyIncome = 1600000;
						maxFamilyIncomeFullGrant = 1000000;
						minGrade = 5.0;
			
						if (!System.IO.File.Exists (path)) {
								// TODO Setea valores por default
								generations = 70000;
								population = 50;
								pCrossover = 0.5;
								pMutation = 0.05;
								maxBudget = 100000000;
						} else {
								// Leo el archivo
								try {
										System.IO.StreamReader reader = new System.IO.StreamReader (path);
										String line;
										while ((line = reader.ReadLine()) !=null) {
												string[] pieces = line.Split ('=');

												if (pieces.Length > 0) {
														if (pieces [0].Contains ("Generaciones"))
																generations = Int32.Parse (pieces [1].Trim ().Replace (".", ""));
														else if (pieces [0].Contains ("Poblacion"))
																population = Int32.Parse (pieces [1].Trim ());
														else if (pieces [0].Contains ("Crossover"))
																pCrossover = Double.Parse (pieces [1].Trim ());
														else if (pieces [0].Contains ("Mutacion"))
																pMutation = Double.Parse (pieces [1].Trim ());
														else if (pieces [0].Contains ("PresupuestoMax"))
																maxBudget = Int32.Parse (pieces [1].Trim ().Replace (".", ""));
												}
										}
										reader.Close ();
								} catch (Exception e) {
										Console.WriteLine (e.Message);
								}
						}
				}
		#endregion
		}
}
