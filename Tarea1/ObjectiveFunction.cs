using System;

namespace UC
{
	public class ObjectiveFunction
	{
		// Suma de los datos
		double totalGrades;
		double totalFamilySE;
		double totalGradesBySE;
		// Datos individuales
		double[] grades;
		double[] familySE;
		// Orden del índice SE, de menor a mayor
		int[] keysFamilySE;
		// Datos en general
		int validCandidates;
		
		public ObjectiveFunction (int maxFamilyIncome, double minGrade)
		{
			this.validCandidates = 0;
			this.totalGrades = 0;
			this.totalFamilySE = 0;
			this.totalGradesBySE = 0;
			
			// Copio los datos
			this.grades = MainClass.data_grades;
			this.familySE = new double[grades.Length];
			this.keysFamilySE = new int[grades.Length];
		
			for (int i=0; i<grades.Length; i++) {
				keysFamilySE [i] = i;
				double SE = MainClass.data_tuitions [i] / MainClass.data_familyIncomes [i];
				familySE [i] = SE; 
				if (MainClass.data_familyIncomes [i] <= maxFamilyIncome && grades [i] >= minGrade) {
					validCandidates++;
					totalFamilySE += SE;
					totalGrades += grades [i];
					totalGradesBySE += grades[i]*SE;
				}
			}
			// Elijo por orden SE al momento de eliminar gente de las becas
			Array.Sort ((double[])familySE.Clone (), keysFamilySE);
		}
		
		/// <summary>
		/// Calcula el fitness del individuo i, y se lo asigna.
		/// </summary>
		/// <param name='i'>
		/// I.
		/// </param>
		public double Fitness (Individual i)
		{
			int[] alloc = i.Chromosome;
			int studentsBenefited = 0;
			double f = 0, gradesBySEAllocated = 0, fundsDistribuited = 0;//, gradesAllocated = 0, familySEAllocated = 0;
			
			for (int s=0; s<grades.Length; s++) {
				if (alloc [s] > 0) {
					fundsDistribuited += MainClass.data_tuitions [s] * (alloc [s] / 2.0);
				}
			}
			
			// Reviso si la solución respeta la única condición que no es controlada antes: el monto máximo de asignación
			int removeStudent;
			for (int j = 0; fundsDistribuited > MainClass.maxBudget && j<keysFamilySE.Length; j++) {
				// Elijo al estudiante con mejor situación SE
				removeStudent = keysFamilySE [j];
				// Si se le asignó beca...
				if (alloc [removeStudent] > 0) {
					// Quitemos beca de forma progresiva
					do{
						// Le quitamos la beca actual
						fundsDistribuited -= (MainClass.data_tuitions [removeStudent] * (alloc[removeStudent] / 2.0));
						// Reducimos la beca
						alloc[removeStudent] = alloc[removeStudent]-1;
						// Le asignamos la nueva beca
						fundsDistribuited += (MainClass.data_tuitions [removeStudent] * (alloc[removeStudent] / 2.0));
					} while(alloc[removeStudent]>0 && fundsDistribuited>MainClass.maxBudget); // Mientras le podamos seguir bajando la beca y se necesite hacerlo
				}
			}
			
			for (int s=0; s<grades.Length; s++) {
				if (alloc [s] > 0) {
					studentsBenefited++;
					//gradesAllocated += this.grades [s];
					//familySEAllocated += this.familySE [s];
					gradesBySEAllocated += this.grades[s]*this.familySE[s];
				}
			}
			
			// TODO Ver cómo funcionan las distintas componentes de la función objetivo	
			// Maximiza el número de estudiantes beneficiados, considerando las notas y la situación SE
			f = gradesBySEAllocated/totalGradesBySE + studentsBenefited/(double)validCandidates;//gradesAllocated / totalGrades + familySEAllocated / totalFamilySE + gradesBySEAllocated/totalGradesBySE + studentsBenefited/(double)validCandidates + fundsDistribuited/MainClass.maxBudget;
			
			// Guardo el valor en el individuo
			i.Fitness = f;
			
			return f;
		}
	}

}

