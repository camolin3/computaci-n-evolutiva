using System;
using System.Collections.Generic;

namespace UC
{
	public class AmountOfEmbassies : IFitnessFunction
	{
		public bool GreaterIsBest {
			get;
			private set;
		}

		public void Fitness (IIndividual i, int ff)
		{
			// Número de embajadas
			double hours = i.Chromosome.Count;
			i.SetFitness (hours, ff);
			GreaterIsBest = true;
		}

		public int Compare (double d1, double d2)
		{
			if (GreaterIsBest)
				return d1.CompareTo (d2);
			else
				return d2.CompareTo (d1);
		}
	}
}

