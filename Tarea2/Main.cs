using System;
using System.IO;
using System.Collections.Generic;

namespace UC
{
	class MainClass
	{
		static int generations;
		static int population;
		static double pCrossover;
		static double pMutation;
		static int tripDuration;

		public static List<byte[]> distances;
		public static List<uint[]> costs;

		public static void Main (string[] args)
		{
			string configPath = (args.Length > 0) ? args [0] : "";
			ReadParametersFile (configPath);
			String distancesPath = (args.Length > 1) ? args [1] : "tiempos.txt";
			ReadDistancesFile (distancesPath);
			String costPath = (args.Length > 2) ? args [2] : "costos.txt";
			ReadCostsFile (costPath);

			InitialReport ();

			IFitnessFunction[] objetives = new IFitnessFunction[]{ new AmountOfEmbassies (), new HourTrip (MainClass.distances)};
			Individual.objetives = objetives;
			Individual.Cities = distances.Count;
			Individual.TripDuration = tripDuration;
			MOGA ga = new MOGA (population, objetives, pCrossover, pMutation, generations);

			HashSet<IIndividual> solutions = ga.Start ();

			int s = 1;
			foreach (var solution in solutions) {
				Console.WriteLine ("Recorrido óptimo {0}:", s);
				Console.WriteLine (solution.ToString ());
				s++;
			}
		}

		private static void InitialReport ()
		{
			Console.WriteLine ("-------------------------------------------");
			Console.WriteLine ("|------Tarea 2 Computación evolutiva------|");
			Console.WriteLine ("|---------Ruta del nuevo ministro---------|");
			Console.WriteLine ("-------------------------------------------");
			Console.WriteLine ();
			
			// Parámetros cargados
			Console.WriteLine ("\tParámetros cargados");
			Console.WriteLine ("\t-------------------");
			Console.WriteLine ("\tGeneraciones:           {0:N}", generations);
			Console.WriteLine ("\tTamaño de la población: {0}", population);
			Console.WriteLine ("\tTasa de crossver:       {0:G}", pCrossover);
			Console.WriteLine ("\tTasa de mutación:       {0:G}", pMutation);
			Console.WriteLine ("\tDías máximos del tour:  {0}", tripDuration);
			Console.WriteLine ();
		}

		#region Reading files...
		/// <summary>
		/// Reads the parameters file.
		/// </summary>
		/// <param name='path'>
		/// Path to the config file.
		/// </param>
		public static void ReadParametersFile (string path)
		{	
			// Cargo los valores por defecto
			generations = 10000;
			population = 50;
			pCrossover = 0.5;
			pMutation = 0.05;
			tripDuration = 10;

			// Si el archivo existe, lo leo
			if (System.IO.File.Exists (path)) {
				try {
					StreamReader reader = new StreamReader (Path.Combine (AppDomain.CurrentDomain.BaseDirectory, path));
					String line;
					while ((line = reader.ReadLine()) !=null) {
						string[] pieces = line.Split ('=');
						
						if (pieces.Length > 0) {
							if (pieces [0].Contains ("Generaciones"))
								generations = Int32.Parse (pieces [1].Trim ().Replace (".", ""));
							else if (pieces [0].Contains ("Poblacion"))
								population = Int32.Parse (pieces [1].Trim ());
							else if (pieces [0].Contains ("Crossover"))
								pCrossover = Double.Parse (pieces [1].Trim ());
							else if (pieces [0].Contains ("Mutacion"))
								pMutation = Double.Parse (pieces [1].Trim ());
							else if (pieces [0].Contains ("DiasTour"))
								tripDuration = Int32.Parse (pieces [1].Trim ());
						}
					}
					reader.Close ();
				} catch (Exception e) {
					Console.WriteLine (e.Message);
				}
			}
		}

		/// <summary>
		/// Reads the data file that contains the distances between cities.
		/// </summary>
		/// <param name='path'>
		/// Name of the file.
		/// </param>
		public static void ReadDistancesFile (string path)
		{
			try {
				string[] lines = File.ReadAllLines (Path.Combine (AppDomain.CurrentDomain.BaseDirectory, path), System.Text.Encoding.GetEncoding ("iso-8859-1"));

				distances = new List<byte[]> (lines.Length);
				
				for (int l=0; l<lines.Length; l++) {
					string[] array = lines [l].Split (',');
					byte[] distancesRow = new byte[array.Length];
					for (int c = 0; c<array.Length; c++)
						distancesRow [c] = Byte.Parse (array [c]);
					distances.Add (distancesRow);
				}
			} catch (Exception e) {
				Console.WriteLine (e.Message);
			}
		}

		/// <summary>
		/// Reads the data file that contains the costs to go between the cities.
		/// </summary>
		/// <param name='path'>
		/// Name of the file.
		/// </param>
		static void ReadCostsFile (string path)
		{
			try {
				string[] lines = File.ReadAllLines (Path.Combine (AppDomain.CurrentDomain.BaseDirectory, path), System.Text.Encoding.GetEncoding ("iso-8859-1"));
				
				costs = new List<uint[]> (lines.Length);
				
				for (int l=0; l<lines.Length; l++) {
					string[] array = lines [l].Split (',');
					uint[] costsRow = new uint[array.Length];
					for (int c = 0; c<array.Length; c++)
						costsRow [c] = UInt32.Parse (array [c]);
					costs.Add (costsRow);
				}
			} catch (Exception e) {
				Console.WriteLine (e.Message);
			}
		}
		#endregion
	}
}
