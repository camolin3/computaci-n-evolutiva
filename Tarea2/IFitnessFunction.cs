using System;
using System.Collections.Generic;

namespace UC
{
	public interface IFitnessFunction : IComparer<double>
	{
		void Fitness (IIndividual i, int ff);

		bool GreaterIsBest {
			get;
		}
	}
}

