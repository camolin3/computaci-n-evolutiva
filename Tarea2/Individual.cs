using System;
using System.Collections.Generic;
using System.Text;

namespace UC
{
	public class Individual : IIndividual
	{
		public static IFitnessFunction[] objetives;
		public static int TripDuration;
		#region Silly static strings
		static string city = "Ciudad";
		static string separator = ", ";
		static string endLabel = ".";
		static string totalCostLabel = "Costo total: $ {0}";
		static string totalTimeLabel = "Tiempo total de viaje: {0} horas";
		#endregion

		List<byte> chromosome;
		List<double> fitness;
		int f;

		#region Properties, getters y setters
		public List<byte> Chromosome {
			get {
				return chromosome;
			}
		}

		public List<double> Fitness {
			get {
				return fitness;
			}
		}

		public int F {
			get {
				return f;
			}
			set {
				f = value;
			}
		}

		public void SetFitness (double fitness, int ff)
		{
			this.fitness [ff] = fitness;
		}

		public byte LastCity {
			get {
				return chromosome [chromosome.Count - 1];
			}
		}

		public static int Cities {
			get;
			set;
		}
		#endregion

		/// <summary>
		/// Generates a randoms solution.
		/// </summary>
		/// <returns>
		/// The solution.
		/// </returns>
		/// <param name='r'>
		/// A random object.
		/// </param>
		public static Individual RandomSolution (Random r)
		{
			// Un día hábil tiene 10 horas. Asumo que el tiempo mínimo de pasar en un consulado es 1 hora.
			// Por lo tanto, en un día se puede visitar un máximo de 5 consulados (asumiendo un tiempo de viaje de una hora, lo mínimo posible).
			// En realidad, es el mínimo entre el día anterior, y la cantidad de ciudades existentes.

			//De esta forma no es necesario mantener control de restricciones ni reparación, si se implementa con cuidado el crossover y la mutación.
			List<byte> newChromosome = new List<byte> (Individual.Cities);
			// El número de ciudades es por lo menos 2, y el máximo que hablábamos recién.
			int numberOfCities = r.Next (2, Math.Min (Individual.Cities, 5 * TripDuration));
			for (int i = 0; i < numberOfCities; i++) {
				byte cityToAdd = Convert.ToByte (r.Next (0, Individual.Cities));
				while (newChromosome.Contains(cityToAdd))
					cityToAdd = Convert.ToByte ((cityToAdd + 1) % Individual.Cities);

				newChromosome.Add (cityToAdd);
			}
			if (newChromosome.Count > Individual.Cities)
				throw new Exception ("Un individuo generado al azar tiene más ciudades en su recorrido de las que existen.");
			return new Individual (newChromosome);
		}

		public Individual (List<byte> chromosome)
		{
			this.chromosome = chromosome;
			fitness = new List<double> (objetives.Length);
			for (int f=0; f<objetives.Length; f++) {
				fitness.Add (0);
			}
		}

		public List<IIndividual> Crossover (IIndividual ind)
		{
			List<IIndividual> result = new List<IIndividual> (2);

			// Creo un struct Crossover
			Crossover c = new Crossover (Cities);

			// Agrego cromosomas de ambos individuos
			c.AddEdges (this);
			c.AddEdges ((Individual)ind);

			result.Add (c.GetChild (this.LastCity));
			result.Add (c.GetChild (((Individual)ind).LastCity));

			return result;
		}

		/// <summary>
		/// Mutate the specified individual via inversion.
		/// </summary>
		/// <param name='r'>
		/// A random number generator.
		/// </param>
		public void Mutate (Random r)
		{
			int pos1 = r.Next (0, chromosome.Count), pos2 = r.Next (0, chromosome.Count);
			// En las posiciones indicadas, hacer inversión
			int min = Math.Min (pos1, pos2);
			int max = Math.Max (pos1, pos2);
			int diff = max - min;

			for (int i=0; i<diff; i++) {
				byte tmp = chromosome [i + min];
				chromosome [i + min] = chromosome [max - i];
				chromosome [max - i] = tmp;
			}
		}

		/// <summary>
		/// Calcules the cost of the solution.
		/// </summary>
		/// <returns>
		/// The cost.
		/// </returns>
		uint CalculeCost ()
		{
			uint cost = 0;

			// Última ciudad visitada
			byte lastCity = LastCity;
			
			foreach (byte city in chromosome) {
				cost += MainClass.costs [lastCity] [city];
				lastCity = city;
			}

			return cost;
		}

		/// <summary>
		/// Calculates the time of the trip.
		/// </summary>
		/// <returns>
		/// The time.
		/// </returns>
		double CalculateTime ()
		{
			// Este valor ya se calculó por HourTrip
			return Fitness [1];
		}

		public bool IsDominatedBy (IIndividual ind)
		{
			Individual i = (Individual)ind;
			return this < i;
		}

		public int Distance (IIndividual ind)
		{
			HashSet<byte> set1 = new HashSet<byte> (this.Chromosome);
			HashSet<byte> set2 = new HashSet<byte> (ind.Chromosome);
			int maxNumberOfCities = Math.Max (this.Chromosome.Count, ind.Chromosome.Count);
			// Intersecto ambos conjuntos
			set1.IntersectWith (set2);
			return maxNumberOfCities - set1.Count; // Retorno la cantidad de ciudades que tienen de diferente
		}

		#region Methods and operators overrides
		public override string ToString ()
		{
			StringBuilder sb = new StringBuilder ();
			for (int c=0; c<chromosome.Count; c++) {
				sb.Append (city);
				sb.Append (chromosome [c] + 1);
				if (c < chromosome.Count - 1)
					sb.Append (separator);
			}
			sb.AppendLine (endLabel);
			sb.AppendLine (String.Format (totalCostLabel, CalculeCost ()));
			sb.AppendLine (String.Format (totalTimeLabel, CalculateTime ()));

			return sb.ToString ();
		}

		public static bool operator > (Individual i1, Individual i2)
		{
			bool greaterOrEqual = i1 >= i2;
			bool greater = false;
			for (int f=0; f<objetives.Length; f++)
				greater = greater || objetives [f].Compare (i1.Fitness [f], i2.Fitness [f]) > 0;
			
			return greaterOrEqual && greater;
		}
		
		public static bool operator < (Individual i1, Individual i2)
		{
			bool lessOrEqual = i1 <= i2;
			bool less = false;
			for (int f=0; f<objetives.Length; f++)
				less = less || objetives [f].Compare (i1.Fitness [f], i2.Fitness [f]) < 0;
			
			return lessOrEqual && less;
		}
		
		public static bool operator >= (Individual i1, Individual i2)
		{
			bool result = true;
			for (int f=0; f<objetives.Length; f++)
				result = result && objetives [f].Compare (i1.Fitness [f], i2.Fitness [f]) >= 0;

			return result;
		}
		
		public static bool operator <= (Individual i1, Individual i2)
		{
			bool result = true;
			for (int f=0; f<objetives.Length; f++)
				result = result && objetives [f].Compare (i1.Fitness [f], i2.Fitness [f]) <= 0;
			
			return result;
		}
		#endregion
	}

	/// <summary>
	/// Struct that facilitates Crossover.
	/// </summary>
	public struct Crossover
	{
		public List<HashSet<byte>> edges1, edges2;
		int cities;
		Random r;

		public Crossover (int numberOfCities)
		{
			edges1 = new List<HashSet<byte>> (numberOfCities);
			edges2 = new List<HashSet<byte>> (numberOfCities);
			for (int c=0; c<numberOfCities; c++) {
				edges1.Add (new HashSet<byte> ());
				edges2.Add (new HashSet<byte> ());
			}
			this.cities = 0;
			r = new Random ();
		}

		public void AddEdges (Individual ind)
		{
			byte lastCity = ind.LastCity;
			foreach (byte city in ind.Chromosome) {
				if (edges1 [city].Count == 0) // Si nunca había ido a esta ciudad
					cities++; // Es una ciudad nueva para mi, y la cuento
				edges1 [lastCity].Add (city);
				edges1 [city].Add (lastCity);
				edges2 [lastCity].Add (city);
				edges2 [city].Add (lastCity);
				lastCity = city;
			}
		}

		public IIndividual GetChild (byte firstCity)
		{
			List<byte> solution = new List<byte> (cities);
			byte cityToAdd = 0;
			int i = firstCity;

			int newSolutionCities = cities;

			while (newSolutionCities>0) {
				if (edges1 [i].Count == 0) {
					// Si ya no quedan caminos, puedo seguir con otra ciudad al azar, o detenerme. Tiro una moneda.
					if (r.NextDouble () <= 0.5) {
						i = r.Next (0, edges1.Count); // Continúo.
						continue;
					} else
						break; // Me salgo. Permite que existan soluciones "cortas".
				}
				int numberOfPaths = edges1 [i].Count;
				int randomPosition = r.Next (0, numberOfPaths);

				int j = 0;
				foreach (byte city in edges1[i]) {
					if (j == randomPosition) {
						cityToAdd = city;
						break;
					}
					j++;
				}

				// Agrego la ciudad escogida a la solución
				solution.Add (cityToAdd);

				// Quito la ciudad ya visitada de entre todas las aristas
				foreach (HashSet<byte> edge in edges1) {
					if (edge.Remove (cityToAdd) && edge.Count == 0)
						newSolutionCities--; // Disminuyo la cantidad de ciudades a visitar
				}



				// La próxima ciudad a revisar será la ciudad a la que viajé
				i = cityToAdd;
			}

			edges1 = edges2; // Dejo listo edges1 para la próxima llamada de GetChild

			return new Individual (solution);
		}
	}
}

