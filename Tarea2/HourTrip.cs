using System;
using System.Collections.Generic;

namespace UC
{
	public class HourTrip : IFitnessFunction
	{
		List<byte[]> distances;
		public bool GreaterIsBest {
			get;
			private set;
		}

		public HourTrip (List<byte[]> distances)
		{
			this.distances = distances;
			GreaterIsBest = false;
		}

		public void Fitness (IIndividual i, int ff)
		{
			// Número de horas de viaje
			double hours = 0;
			
			// Última ciudad visitada
			byte lastCity = i.Chromosome [i.Chromosome.Count - 1];
			
			foreach (byte city in i.Chromosome) {
				hours += distances [lastCity] [city];
				lastCity = city;
			}
			i.SetFitness (hours, ff);
		}

		public int Compare (double d1, double d2)
		{
			if (GreaterIsBest)
				return d1.CompareTo (d2);
			else
				return d2.CompareTo (d1);
		}
	}
}

