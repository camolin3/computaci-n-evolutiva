using System;
using System.Collections.Generic;

namespace UC
{
	public interface IIndividual
	{
		List<byte> Chromosome {
			get;
		}

		List<double> Fitness {
			get;
		}

		int F {
			get;
			set;
		}

		void SetFitness (double fitness, int ff);

		bool IsDominatedBy (IIndividual ind);

		int Distance (IIndividual ind);
	}
}

