using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace UC
{
	/// <summary>
	/// Multi-Objective Genetic Algorithm Class.
	/// </summary>
	public class MOGA
	{
		Random r;
		IFitnessFunction[] objetives;
		double pCrossover, pMutation;
		IIndividual[] oldPop, newPop;
		int generation, maxGenerations;
		List<IIndividual> nonDominatedFront;

		protected Stopwatch sw;

		public MOGA (int popSize, IFitnessFunction[] objetives, double pCrossover, double pMutation, int maxGenerations)
		{
			r = new Random ();
			sw = new Stopwatch ();
			this.objetives = objetives;
			this.pCrossover = pCrossover;
			this.pMutation = pMutation;
			generation = 0;
			this.maxGenerations = maxGenerations;
			oldPop = new IIndividual[popSize];
			newPop = new IIndividual[popSize];
			// Creo los individuos aleatorios de la población inicial
			for (int i = 0; i < popSize; i++) {
				oldPop [i] = Individual.RandomSolution (r);
			}
		}

		public HashSet<IIndividual> Start ()
		{
			Console.WriteLine ("Inicio del algoritmo genético multi-objetivo");
			Console.WriteLine ("--------------------------------------------");
			sw.Start ();

			do {
				generation++;
				Generation ();
				Report ();
				oldPop = newPop;
			} while(Continue());

			sw.Stop ();

			Console.WriteLine ("--------------------------------------------------");
			Console.WriteLine ("Finalización del algoritmo genético multi-objetivo");
			Console.WriteLine ("Tiempo total de ejecución: {0} minutos, {1} segundos", sw.Elapsed.Minutes, sw.Elapsed.Seconds);
			Console.WriteLine ();

			HashSet<IIndividual> PO = new HashSet<IIndividual> (nonDominatedFront, new IndividualEqualityComparer ());

			return PO; // Retorno el último conjunto no-dominado encontrado.
		}

		protected void EvaluatePopulation ()
		{
			foreach (IIndividual i in oldPop)
				for (int o = 0; o<objetives.Length; o++)
					objetives [o].Fitness (i, o);
		}

		/// <summary>
		/// Select an IIndividual at random.
		/// </summary>
		protected IIndividual Select ()
		{
			// El elegido
			IIndividual theOne;

			// Esta vez usaré sólo torneo
			int p1 = r.Next (0, oldPop.Length), p2;
			do {
				p2 = r.Next (0, oldPop.Length);
			} while(p1==p2);
			theOne = oldPop [p2].IsDominatedBy (oldPop [p1]) ? oldPop [p1] : oldPop [p2];

			return theOne;
		}

		/// <summary>
		/// Creates a new generation by evaluating the old one, making a non-dominated order, and doing a classic GA using the dummy fitness value.
		/// </summary>
		protected void Generation ()
		{
			// Evalúo la población
			EvaluatePopulation ();

			for (int i = 0; i < oldPop.Length; i++) {
				oldPop [i].F = -1;
			}

			int dummyValue = Int32.MaxValue; // Parto asignando un valor tonto, pero alto
			int solutionsRemaining = oldPop.Length;
			while (solutionsRemaining>0) {
				// Busco las soluciones en este nivel de no-dominación
				List<IIndividual> noDomination = new List<IIndividual> (solutionsRemaining);
				if (solutionsRemaining == oldPop.Length)
					nonDominatedFront = noDomination; // Si estamos en la primera iteración tendremos el frente no-dominado. Guardo la referencia.

				// Para cada uno de los individuos de la vieja población se compara con todo el resto en no-dominancia.
				for (int i=0; i<oldPop.Length; i++) {
					if (oldPop [i].F > 0)
						continue; // Si ya fue clasificado: ¡Siguiente!
					bool nonDominated = true;
					for (int j=0; j<oldPop.Length && nonDominated; j++) {
						if (oldPop [j].F > 0 || i == j)
							continue; // Si ya fue clasificado o se trata de la misma solución: ¡Siguiente!
						if (oldPop [i].IsDominatedBy (oldPop [j]))
							nonDominated = false;
					}
					if (nonDominated) {
						oldPop [i].F = dummyValue;
						noDomination.Add (oldPop [i]);
						solutionsRemaining--; // Me queda una solución menos
					}
				}
				// Ahora que tengo las soluciones no-dominadas, prefiero óptimos suficientemente distintos
				// Calculo las distancias entre las soluciones
				for (int i = 0; i < noDomination.Count; i++) {
					double sumSharing = 0.0;
					for (int j = 0; j < noDomination.Count; j++) {
						double sharing = Sharing (noDomination [i].Distance (noDomination [j]));
						if (i == j && sharing != 1)
							throw new Exception ("¡Esto no puede ocurrir! El sharing de dist(x,x) debe ser siempre 1.");
						sumSharing += sharing;
					}
					// Evaluación compartida
					noDomination [i].F = Convert.ToInt32 (noDomination [i].F / sumSharing);
					// Siempre busco el menor F asignado para el próximo tonto valor de no-dominación
					dummyValue = Math.Min (dummyValue, noDomination [i].F);
				}
			}

			// Seguimos... ahora hacemos como si fuese un GA tradicional
			List<IIndividual> childrens;
			for (int i = 0; i < newPop.Length; i+=childrens.Count) {
				// Suponemos la población es par
				IIndividual i1 = Select (), i2;
				do {
					i2 = Select ();
				} while( Object.ReferenceEquals(i1, i2));

				// Para los dos padres se realiza crossover con una cierta probabilidad
				if (r.NextDouble () <= pCrossover)
					childrens = ((Individual)i1).Crossover (i2);
				else {
					// Si no hay crossover, pasan directo
					childrens = new List<IIndividual> (2);
					// Agrego NUEVOS individuos; así me aseguro que sean todos distintos
					childrens.Add (new Individual (i1.Chromosome));
					childrens.Add (new Individual (i2.Chromosome));
				}

				for (int j = 0; j < childrens.Count; j++) {
					// Con una cierta probabilidad los individuos sufren mutaciones
					if (r.NextDouble () <= pMutation)
						((Individual)childrens [j]).Mutate (r);
				}

				// En este punto sólo nos queda agregar los hijos a la nueva generación
				for (int j = 0; j < childrens.Count; j++) {
					newPop [i + j] = childrens [j];
				}
			}
		}

		protected void Report ()
		{
			if (generation % 250 == 0) // Digo lo único que se me ocurre decir
				Console.WriteLine ("{2}% Cantidad de individuos en el primer frente de no-dominación: {0}/{1}", nonDominatedFront.Count, oldPop.Length, 100 * generation / maxGenerations);
		}

		protected bool Continue ()
		{
			return generation < maxGenerations;
		}

		double Sharing (int distance)
		{
			if (distance > Individual.Cities)
				throw new ArgumentException ("Distancia supera la cantidades de ciudades", "distance");
			double sharing = (Individual.Cities - distance) / (double)Individual.Cities;
			return sharing;
		}
	}

	public class IndividualEqualityComparer : IEqualityComparer<IIndividual>
	{
		public bool Equals (IIndividual i1, IIndividual i2)
		{
			return i1.Distance (i2) == 0;
		}

		public int GetHashCode (IIndividual i)
		{
			int hash = 0;
			for (int j = 0; j < i.Chromosome.Count; j++) {
				hash ^= i.Chromosome [j];
			}
			return hash.GetHashCode ();
		}
	}
}

